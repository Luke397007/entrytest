package FirstTask;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Parser {

    private final static File SOURCE = new File("src/FirstTask/source.txt");
    private final static String FILE_PATH = SOURCE.getPath();

    public static void main(String[] args) {

        try (Stream<String> stream = Files.lines(Paths.get(FILE_PATH), Charset.forName("UTF-8"))) {

            stream.flatMap(Pattern.compile("[.,/ -;:]")::splitAsStream).map(String::toLowerCase).distinct().forEach(System.out::println);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
