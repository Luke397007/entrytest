package SecondTask;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class MoveArrayToCollection {

    public static void main(String[] args) {
        String[] arrayData = {"My aspirations", "to get", "new Java EE", "skills"};
        List<String> list = new ArrayList<>();
        convertArrayToCollection(arrayData, list);
    }

    /**
     *Converts Array to Collection
     * @param arrayData
     * @param collection
     * @param <T>
     */
    private static <T> void convertArrayToCollection(T[] arrayData, Collection<T> collection) {
        try {
            if (arrayData.length != 0) {
                Collections.addAll(collection, arrayData);
                collection.forEach(System.out::println);
            } else {
                System.out.println("Array is empty");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
