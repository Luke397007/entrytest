package ThirdTask;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class ReentrantLockVersion {

    public static void main(String[] args) throws InterruptedException {

        final Task task = new Task();
        Thread thread1 = new Thread(task::getNextValue);
        Thread thread2 = new Thread(task::getNextValue);

        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();

        task.showCounter();
    }
}

class Task {

    private int value;
    private Lock lock = new ReentrantLock();

    /**
     * gets unique value using ReentrantLock class
     */
    void getNextValue() {
        lock.lock();
        for (int i = 0; i < 100; i++) {
            value++;
        }
        lock.unlock();
    }

    /**
     * prints incremented value
     */
    void showCounter() {
        System.out.println(value);
    }
}