package ThirdTask;

public class SynchronizedVersion {

    private int value = 0;

    public static void main(String[] args) throws InterruptedException {
        SynchronizedVersion sync = new SynchronizedVersion();

        for (int i = 0; i < 100; i++)
            sync.incrementation();
    }

    /**
     * gets unique value using synchronized keyword
     */
    private synchronized void getNextValue() {
        value++;
        System.out.println(value);
    }

    /**
     * creates threads to get unique incremented value
     * @throws InterruptedException
     */
    private void incrementation() throws InterruptedException {
        Thread thread1 = new Thread(this::getNextValue);
        Thread thread2 = new Thread(this::getNextValue);

        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();
    }
}
