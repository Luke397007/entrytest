package ThirdTask;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class AtomicIntegerVersion {

    public static void main(String[] args) throws InterruptedException {

        final Increment increment = Increment.getConnection();

        /**
         * gets unique values using thread pool
         */
        ExecutorService executor = Executors.newFixedThreadPool(10);
        for (int i = 0; i < 100; i++)
            executor.submit(increment::getValue);
        executor.shutdown();
        executor.awaitTermination(1, TimeUnit.DAYS);
    }
}

class Increment {

    private int value = 0;
    private AtomicInteger atomicInteger = new AtomicInteger(value);
    private static Increment increment = new Increment();

    /**
     * gets static object
     * @return
     */
    static Increment getConnection() {
        return increment;
    }

    /**
     * gets unique value and print it
     */
    void getValue() {
        System.out.println(atomicInteger.incrementAndGet());
    }
}
